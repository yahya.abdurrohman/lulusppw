from django.shortcuts import render, redirect
from .models import Mata_Kuliah
from .forms import Matkul_Form

def home(request):
    return render(request, 'main/home.html')

def profile(request):
    return render(request, 'main/profile.html')

def halamanA(request):
    return render(request, 'main/halamanA.html')

def formMatkul(request):
    
    form = Matkul_Form(request.POST or None)
    nilai = False
    if(form.is_valid and request.method == 'POST'):
        form.save()
        nilai = True
    
    matkul = Mata_Kuliah.objects.all().last()
    context = {
        'form': form,
        'nilai': nilai,
        'matkul': matkul
    }
    return render(request, 'main/formMatkul.html', context)

def daftarMatkul(request):
    matkuls = Mata_Kuliah.objects.all()
    context = {
        'matkuls': matkuls,
    }
    return render(request, 'main/daftarMatkul.html', context)

def deleteItems(request, id):
    item = Mata_Kuliah.objects.get(id=id)
    item.delete()
    return redirect('daftarMatkul/')

def detailMatkul(request, id):
    info_matkul = Mata_Kuliah.objects.get(id=id)
    context = {
        'info_matkul': info_matkul,
    }
    return render(request, 'main/detailMatkul.html', context)

    




    
