from django.urls import path, re_path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('halamanA/', views.halamanA, name='halamanA'),
    path('formMatkul/', views.formMatkul, name='formMatkul'),
    path('daftarMatkul/', views.daftarMatkul, name='daftarMatkul'),
    re_path(r'^(?P<id>[0-9]+)$', views.deleteItems, name='deleteItems'),
    path('detailMatkul/<id>', views.detailMatkul, name='detailMatkul')
   
]
