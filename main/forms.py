from django import forms
from .models import Mata_Kuliah


class Matkul_Form(forms.ModelForm):
    class Meta:
        model = Mata_Kuliah
        fields = ['Nama_matkul', 'Dosen_pengajar', 'Jumlah_sks', 'Semester_tahun', 'Ruang_kelas', 'Deskripsi_matkul']

    Nama_matkul = forms.CharField(label='Nama Matkul', required=True, max_length=27, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'masukan nama matkul'}))
    Dosen_pengajar = forms.CharField(label='Dosen Pengajar',max_length=27, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text'}))
    Jumlah_sks = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    Semester_tahun = forms.CharField(label='Semester Tahun',max_length=27, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text'}))
    Ruang_kelas = forms.CharField(label='Ruang Kelas',max_length=27, widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text'}))
    Deskripsi_matkul = forms.CharField(label='Deskripsi Matkul', required=True, max_length=100, widget=forms.Textarea(attrs={'class': 'form-control'}))
    

